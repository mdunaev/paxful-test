# Test task for Paxful

Live version - [http://46.101.248.7](http://46.101.248.7)

Start in development mode on localhost:

- Run `npm i && npm start` in `/server` folder
- Run `npm i && npm start` in `/frontend` folder in another terminal session
  Project server will start on `loaclhost:3000`

Start project in Docker Container:

- Run `npm run rebuild` in project root folder.
