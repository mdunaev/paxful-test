#!/bin/bash
clear
echo "Paxful test task. Redeploying..."
git stash
git pull
cd frontend
npm install
npm run build
cd ..
cd server
npm i
cd ..
docker rm $(docker ps -a -q)
docker rmi $(docker images -q)
docker-compose up
