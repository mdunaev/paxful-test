import './App.css';

import { ConnectedRouter } from 'connected-react-router';
import React, { Component } from 'react';
import { Route } from 'react-router';

import { AppLayout } from './components/AppLayout/AppLayout';

interface AppProps {
  history: any;
}
const App = ({ history }: AppProps) => {
  return (
    <ConnectedRouter history={history}>
      <Route path="*" component={AppLayout} />
    </ConnectedRouter>
  );
};

export default App;
