import React from 'react';

import styles from './Avatar.module.css';

interface AvatarProps {
  userId: string;
}
export const Avatar = ({ userId }: AvatarProps) => (
  <div
    className={styles.avatar}
    style={{
      backgroundImage: `url(https://www.gravatar.com/avatar/${userId}?d=identicon)`
    }}
  />
);
