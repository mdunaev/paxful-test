import React from 'react';
import { connect } from 'react-redux';

import { ChatMessage, State } from '../../store/initialState';
import { selectCurrentChatMessages, selectCurrentUserId, selectTradeId, selectUserByIdFn } from '../../store/selectors';
import styles from './Chat.module.css';
import { ChatHeader } from './ChatHeader/ChatHeader';
import { ChatInput } from './ChatInput/ChatInput';
import { Message } from './Message/Message';

interface ChatProps {
  messages: ChatMessage[];
  userId: string;
  tradeId: string;
  getUserById: Function;
}

const ChatComponent = ({
  messages,
  getUserById,
  userId,
  tradeId
}: ChatProps) => {
  return (
    <div className={styles.chat}>
      <ChatHeader />
      <div className={styles.messages}>
        <div className={styles.messagesScrollWrapper}>
          {messages.map((message: ChatMessage, i: number) => (
            <Message
              key={message.text + tradeId}
              userName={getUserById(message.fromId).name}
              userId={message.fromId}
              isUserMessage={userId === message.fromId}
              date={message.date}
              text={message.text}
              isImage={message.isImage}
              tradeId={tradeId}
            />
          ))}
        </div>
      </div>
      <div className={styles.input}>
        <ChatInput />
      </div>
    </div>
  );
};

const mapProps = (state: State) => ({
  messages: selectCurrentChatMessages(state),
  userId: selectCurrentUserId(state),
  getUserById: selectUserByIdFn(state),
  tradeId: selectTradeId(state)
});

export const Chat = connect(mapProps)(ChatComponent);
