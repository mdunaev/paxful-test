import React, { ChangeEvent, FormEvent } from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { chatInputChangedAction, sendImageAction, sendMessageAction } from '../../../store/actions';
import { State } from '../../../store/initialState';
import { selectChatMessage, selectIsImageSending } from '../../../store/selectors';
import styles from './ChatInput.module.css';
import imageIcon from './image.svg';

interface ChatInputProps {
  changeChatMessage: Function;
  sendMessage: Function;
  sendImage: Function;
  chatMessage: string;
  isImageSending: boolean;
}
export const ChatInputComponent = ({
  changeChatMessage,
  sendMessage,
  chatMessage,
  sendImage,
  isImageSending
}: ChatInputProps) => {
  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    changeChatMessage(event.target.value);
  };

  const onSubmit = (event: FormEvent<HTMLFormElement>) => {
    sendMessage();
    event.preventDefault();
  };
  const onFileChange = (event: FormEvent<HTMLInputElement>) => {
    const files: FileList = (event.target as any).files;
    if (files && files[0]) {
      sendImage(files[0]);
    }
    event.preventDefault();
  };
  const clipClasses = isImageSending
    ? `${styles.clip} ${styles.sending}`
    : styles.clip;
  return (
    <form className={styles.chatinput} onSubmit={onSubmit}>
      <div className={clipClasses}>
        <input
          type="file"
          name="file"
          id="file"
          onChange={onFileChange}
        />
        <label htmlFor="file">
          <img src={imageIcon} />
        </label>
      </div>
      <div className={styles.inputWrapper}>
        <input
          className={styles.input}
          value={chatMessage}
          type="text"
          placeholder="Type your message..."
          onChange={onChange}
        />
      </div>
      <div className={styles.send}>
        <input type="submit" value="SEND" />
      </div>
    </form>
  );
};

const mapProps = (state: State) => ({
  chatMessage: selectChatMessage(state),
  isImageSending: selectIsImageSending(state)
});
const mapDispatch = (dispatch: Dispatch) => ({
  sendImage: (file: File) => dispatch(sendImageAction(file)),
  changeChatMessage: (message: string) =>
    dispatch(chatInputChangedAction(message)),
  sendMessage: () => dispatch(sendMessageAction())
});

export const ChatInput = connect(
  mapProps,
  mapDispatch
)(ChatInputComponent);
