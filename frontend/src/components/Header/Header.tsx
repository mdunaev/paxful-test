import React from 'react';

import styles from './Header.module.css';
import { MobileMenu } from './MobileMenu/MobileMenu';

export const Header = () => (
  <div>
    <div className={styles.header}>
      <div className={styles.logo}>PΛXFUL</div>
      <div className={styles.menu}>
        <div className={styles.menuItem}>Buy bitcoins</div>
        <div className={styles.menuItem}>Sell bitcoins</div>
        <div className={styles.menuItem}>Wallet</div>
        <div className={styles.menuItem}>Support</div>
        <div className={styles.menuItem}>Your account</div>
      </div>
    </div>
    <div className={styles.subheader}>
      <div className={styles.subheaderItem}>Overview</div>
      <div className={styles.subheaderItem}>Trades</div>
      <div className={styles.subheaderItem}>Disputes</div>
      <div className={styles.subheaderItem}>Your offers</div>
      <div className={styles.subheaderItem}>My team</div>
      <div className={styles.subheaderItem}>Trade History</div>
    </div>
    <MobileMenu />
  </div>
);
