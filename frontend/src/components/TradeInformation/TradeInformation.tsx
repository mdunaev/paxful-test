import { formatDistance } from 'date-fns';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { payTradeAction } from '../../store/actions';
import { State, Trade, User } from '../../store/initialState';
import { selectBTCPrice, selectCurrentTrade, selectTraderUser } from '../../store/selectors';
import { Avatar } from '../Avatar/Avatar';
import styles from './TradeInformation.module.css';

interface TradeInformationProps {
  trader: User;
  trade: Trade;
  btcPrice: number;
  releaseBTC: Function;
}

const TradeInformationComponent = ({
  trader,
  trade,
  btcPrice,
  releaseBTC
}: TradeInformationProps) => {
  const releasehandler = () => {
    releaseBTC(trade.tradeId);
  };
  return (
    <div className={styles.tradeinfo}>
      <div className={styles.userInfo}>
        You are trading with <br />
        <b>
          <a
            className={styles.avatar}
            href={`/${trader.id}/${trade.tradeId}/`}
          >
            {trader.name}
          </a>
        </b>
      </div>
      <div className={styles.date}>
        Started {formatDistance(new Date(trade.date), new Date())}
      </div>
      <div
        className={`${styles.release} ${trade.isPaid &&
          styles.payed}`}
        onClick={releasehandler}
      >
        Release Bitcoins
      </div>
      <div className={styles.table}>
        <div className={styles.userInfo}>
          <Avatar userId={trader.id} />
          <div className={styles.rating}>
            <span className={styles.positive}>
              +{trader.positiveReputation}
            </span>

             /
            <span className={styles.negative}>
               {trader.negativeReputation}
            </span>
          </div>
        </div>
        <div className={styles.status}>
          <div className={styles.tableHeader}>TRADE STATUS</div>
          {trade.isPaid ? 'paid' : 'not paid'}
        </div>
        <div className={styles.hash}>
          <div className={styles.tableHeader}>TRADE HASH</div>
          {trade.tradeId}
        </div>
        <div className={styles.amount}>
          <div className={styles.tableHeader}>AMOUNT</div>
          {trade.amount} USD
          <br />
          {(trade.amount / btcPrice).toString().substring(0, 9)} BTC
        </div>
      </div>

      <div className={styles.footerItem}>
        <div className={styles.footerHeader}>PAYMENT METHOD NAME</div>
        {trade.paymentMethod}
      </div>
      <div className={styles.footerItem}>
        <div className={styles.footerHeader}>BTC/USD RATE</div>
        {btcPrice}
      </div>
      <div className={styles.footerItem}>
        <div className={styles.footerHeader}>LINK TO YOUR OFFER</div>
        <a href={window.location.href}>{window.location.href}</a>
      </div>
    </div>
  );
};

const mapProps = (state: State) => ({
  trader: selectTraderUser(state),
  trade: selectCurrentTrade(state),
  btcPrice: selectBTCPrice(state)
});
const mapDispatch = (dispatch: Dispatch) => ({
  releaseBTC: (tradeId: string) => dispatch(payTradeAction(tradeId))
});

export const TradeInformation = connect(
  mapProps,
  mapDispatch
)(TradeInformationComponent);
