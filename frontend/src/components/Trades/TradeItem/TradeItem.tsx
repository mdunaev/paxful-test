import { formatDistance } from 'date-fns';
import React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { changeTradeAction } from '../../../store/actions';
import { State, Trade, User } from '../../../store/initialState';
import { selectIsUserBuyer } from '../../../store/selectors';
import { Avatar } from '../../Avatar/Avatar';
import styles from './TradeItem.module.css';

interface TradeItemProps {
  btcPrice: number;
  user: User;
  trade: Trade;
  selectTrade: (id: string) => void;
  selected: boolean;
  userIsBuyer: boolean;
}
const TradeItemComponent = ({
  btcPrice,
  user,
  trade,
  selectTrade,
  selected,
  userIsBuyer
}: TradeItemProps) => {
  const indicatorClassName = trade.hasNewMessages
    ? styles.greenbg
    : styles.graybg;
  return (
    <div
      className={`${styles.tradeitem} ${selected && styles.selected}`}
      onClick={() => selectTrade(trade.tradeId)}
    >
      <div className={styles.leftColumn}>
        <div
          className={`${styles.indicator} ${indicatorClassName}`}
        />
        <div className={styles.name}>
          {user.name} {userIsBuyer ? 'is selling' : 'is buying'}
        </div>
      </div>

      <div className={styles.centralColumn}>
        <div className={styles.payment}>{trade.paymentMethod}</div>
        <div className={styles.amount}>
          {trade.amount} USD (
          {(trade.amount / btcPrice).toString().substring(0, 10)}
           BTC)
          <div className={styles.date}>
            {formatDistance(new Date(trade.date), new Date())}
             ago
          </div>
        </div>
      </div>
      <div className={styles.rightColumn}>
        <Avatar userId={user.id} />
        <div
          className={`${styles.isPayed} ${trade.isPaid &&
            styles.payed}`}
        >
          {trade.isPaid ? 'paid' : 'not paid'}
        </div>
      </div>
      <br />
    </div>
  );
};

const mapProps = (state: State) => ({
  userIsBuyer: selectIsUserBuyer(state)
});

const mapDispatch = (dispatch: Dispatch) => ({
  selectTrade: (id: string) => dispatch(changeTradeAction(id))
});

export const TradeItem = connect(
  mapProps,
  mapDispatch
)(TradeItemComponent);
