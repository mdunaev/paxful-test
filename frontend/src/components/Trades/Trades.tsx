import React from 'react';
import { connect } from 'react-redux';

import { State, Trade } from '../../store/initialState';
import {
  selectBTCPrice,
  selectFilteredTrades,
  selectIsUserBuyer,
  selectTradeId,
  selectUserByIdFn,
} from '../../store/selectors';
import { UserPanel } from '../UserPanel/UserPanel';
import { TradeFilter } from './TradeFilter/TradesFilter';
import { TradeItem } from './TradeItem/TradeItem';
import styles from './Trades.module.css';

interface TradesProps {
  trades: Trade[];
  getUserById: Function;
  btcPrice: number;
  selectedTradeId: string;
  userIsBuyer: boolean;
}
const TradesComponent = ({
  trades,
  getUserById,
  btcPrice,
  selectedTradeId,
  userIsBuyer
}: TradesProps) => (
  <div className={styles.trades}>
    <UserPanel />
    <TradeFilter />
    <div className={styles.itemsScroll}>
      {trades.map((trade: Trade, i: number) => (
        <TradeItem
          key={i}
          btcPrice={btcPrice}
          user={getUserById(
            userIsBuyer ? trade.sellerId : trade.buyerId
          )}
          trade={trade}
          selected={selectedTradeId === trade.tradeId}
        />
      ))}
    </div>
  </div>
);

const mapProps = (state: State) => ({
  trades: selectFilteredTrades(state),
  getUserById: selectUserByIdFn(state),
  btcPrice: selectBTCPrice(state),
  selectedTradeId: selectTradeId(state),
  userIsBuyer: selectIsUserBuyer(state)
});

export const Trades = connect(mapProps)(TradesComponent);
