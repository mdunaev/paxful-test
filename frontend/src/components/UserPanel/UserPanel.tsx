import React from 'react';
import { connect } from 'react-redux';

import { State } from '../../store/initialState';
import { selectCurrentUserId, selectCurrentUserName, selectUserRole } from '../../store/selectors';
import { Avatar } from '../Avatar/Avatar';
import styles from './UserPanel.module.css';

interface UserPanelProps {
  username: string;
  userrole: string;
  userId: string;
}
const UserPanelComponent = ({
  username,
  userrole,
  userId
}: UserPanelProps) => (
  <div className={styles.userpanel}>
    <Avatar userId={userId} />
    <div className={styles.nameContainer}>
      <div className={styles.curent}>Current Logged User:</div>
      <div className={styles.username}>{username}</div>
      <div className={styles.userrole}>{userrole}</div>
    </div>
  </div>
);

const mapProps = (state: State) => ({
  username: selectCurrentUserName(state),
  userrole: selectUserRole(state),
  userId: selectCurrentUserId(state)
});
export const UserPanel = connect(mapProps)(UserPanelComponent);
