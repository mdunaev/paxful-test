import { push } from 'connected-react-router';
import { Dispatch } from 'redux';

import { TradesFilterTypes } from '../components/Trades/TradeFilter/TradesFilter';
import { ChatMessage, ServerState, State, Trade } from './initialState';
import { selectRouterData } from './selectors';
import { deleteTradeSocket, markTradeAsReadSocket, payTradeSocket, sendMessageSocket } from './transport';

// Thunk
// If trade id from router exist in new state
// then dispatch new state and change roter path
export const REFRESH_STATE = 'REFRESH STATE';
export const refreshStateAction = (newState: ServerState) =>
  ((dispatch: Dispatch, getState: () => State) => {
    if (newState.trades.length === 0) {
      location.href = '/';
      return;
    }
    const state = getState();
    const oldTradeId = selectRouterData(state).tradeId;
    const newTradeId = newState.trades[0].tradeId;
    const isOldTradeIdValid = newState.trades.some(
      (trade: Trade) => trade.tradeId === oldTradeId
    );
    const originalTradeId = isOldTradeIdValid
      ? oldTradeId
      : newTradeId;
    dispatch({
      type: REFRESH_STATE,
      newState,
      newTradeId: originalTradeId
    });
    dispatch(push(`/${newState.id}/${originalTradeId}/`));
  }) as any;

export const UPDATE_BTC_PRICE = 'UPDATE BTC PRICE';
export const updateBTCPriceAction = (price: number) => ({
  type: UPDATE_BTC_PRICE,
  price
});

// Thunk
// Dispatch action, change router path, mark as read on server side
export const CHANGE_TRADE = 'CHANGE TRADE';
export const changeTradeAction = (tradeId: string) =>
  ((dispatch: Dispatch, getState: () => State) => {
    dispatch({
      type: CHANGE_TRADE,
      tradeId
    });
    dispatch(push(`/${getState().serverState.id}/${tradeId}/`));
    markTradeAsReadSocket(tradeId);
  }) as any;

export const CHAT_INPUT_CHANGED = 'CHAT INPUT CHANGED';
export const chatInputChangedAction = (text: string) => ({
  type: CHAT_INPUT_CHANGED,
  text
});

// Thunk
// Check message length
// Dispatch action and send message to server
export const SEND_MESSAGE = 'SEND MESSAGE';
export const sendMessageAction = () =>
  ((dispatch: Dispatch, getState: () => State) => {
    const state = getState();
    if (state.chatMessage.length === 0) return;
    dispatch({
      type: SEND_MESSAGE,
      text: state.chatMessage
    });

    sendMessageSocket(
      state.chatMessage,
      state.selectedTradeId,
      state.serverState.id
    );
  }) as any;

// Thunk
// Dispatch Action
// Send file to Cloudinary
// After response send message to server
export const SEND_IMAGE = 'SEND IMAGE';
export const sendImageAction = (file: File) =>
  ((dispatch: Dispatch, getState: () => State) => {
    dispatch({
      type: SEND_IMAGE
    });
    const formData = new FormData();
    formData.append('file', file);
    formData.append('upload_preset', 'sme17ylc');
    fetch('https://api.cloudinary.com/v1_1/dkt7dxgii/image/upload', {
      method: 'POST',
      body: formData
    })
      .then((result: Response) => result.json())
      .then(data => {
        const state = getState();
        sendMessageSocket(
          data.url,
          state.selectedTradeId,
          state.serverState.id,
          true
        );
      })
      .catch(err => {
        console.log('image uploading error - ', err);
      });
  }) as any;

export const NEW_MESSAGE = 'NEW MESSAGE';
export const newMessageAction = (
  message: ChatMessage & { tradeId: string }
) => ({
  type: NEW_MESSAGE,
  message
});

export const DISCONNECT = 'DISCONNECT';
export const disconnectAction = () => ({
  type: DISCONNECT
});

// Thunk
// Dispatch Action
// Notify server
export const DELETE_TRADE = 'DELETE TRADE';
export const deleteTradeAction = (tradeId: string) =>
  ((dispatch: Dispatch) => {
    dispatch({
      type: DELETE_TRADE,
      tradeId
    });
    deleteTradeSocket(tradeId);
  }) as any;

// Thunk
// Dispatch Action
// Notify server
export const PAY_TRADE = 'PAY TRADE';
export const payTradeAction = (tradeId: string) =>
  ((dispatch: Dispatch) => {
    dispatch({
      type: PAY_TRADE,
      tradeId
    });
    payTradeSocket(tradeId);
  }) as any;

export const CHANGE_TRADES_FILTER = 'CHANGE TRADES FILTER';
export const changeFitlerAction = (newFilter: TradesFilterTypes) => ({
  type: CHANGE_TRADES_FILTER,
  newFilter
});

export enum MobileMenuTypes {
  list = 'Trades List',
  chat = 'Chat',
  info = 'Trade Info'
}
export const CHANGE_MOBILE_MENU = 'CHANGE MOBILE MENU';
export const changeMobileMenuAction = (menu: MobileMenuTypes) => ({
  type: CHANGE_MOBILE_MENU,
  menu
});
