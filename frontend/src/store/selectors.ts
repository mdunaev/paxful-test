import { createSelector } from 'reselect';

import { TradesFilterTypes } from '../components/Trades/TradeFilter/TradesFilter';
import { State, Trade, User } from './initialState';

export const selectTrades = (state: State): Trade[] =>
  state.serverState.trades;

export const selectTradesFilter = (state: State) =>
  state.selectedFilter;

export const selectMobileMenu = (state: State) => state.mobileMenu;

const sortTradesByTime = (trades: Trade[]) =>
  trades.sort(
    (firstTrade: Trade, secondTrade: Trade) =>
      new Date(secondTrade.date).getTime() -
      new Date(firstTrade.date).getTime()
  );

export const selectCurrentUser = (state: State): User => {
  const id = state.serverState.id;
  return state.serverState.buyers
    .concat(state.serverState.sellers)
    .filter((user: User) => user.id === id)[0];
};

export const selectAllUsers = (state: State) => [
  ...state.serverState.buyers,
  ...state.serverState.sellers
];

export const selectIsDataLoaded = (state: State): boolean =>
  state.serverState.trades.length > 0;

export interface RouterData {
  userId: string;
  tradeId: String;
}
export const selectRouterData = (state: any): RouterData => {
  const [_, userId, tradeId] = state.router.location.pathname.split(
    '/'
  );
  return {
    userId,
    tradeId
  };
};

export const selectBTCPrice = (state: State): number =>
  state.btcPrice;

export const selectUserByIdFn = (state: State): Function => (
  id: string
): User | {} =>
  [...state.serverState.buyers, ...state.serverState.sellers].reduce(
    (acc: {}, current: User) => (current.id === id ? current : acc),
    {}
  );

export const selectTradeId = (state: State) => state.selectedTradeId;

export const selectIsDisconnect = (state: State) =>
  state.isDisconnect;

export const selectCurrentTrade = (state: State) =>
  state.serverState.trades.reduce((acc: any, trade: Trade) => {
    return trade.tradeId === state.selectedTradeId ? trade : acc;
  });

export const selectTradeById = (state: State, tradeId: string) =>
  state.serverState.trades.reduce((acc: any, trade: Trade) => {
    return trade.tradeId === tradeId ? trade : acc;
  });

export const selectChatMessage = (state: State) => state.chatMessage;
export const selectIsImageSending = (state: State) =>
  state.isImageSending;

export const selectFilteredTrades = createSelector(
  selectTrades,
  selectTradesFilter,
  (trades: Trade[], filterType: any) => {
    const filterValue = TradesFilterTypes[filterType];
    const tradesByTime = sortTradesByTime(trades);

    switch (filterValue) {
      case TradesFilterTypes.last:
        return tradesByTime;
      case TradesFilterTypes.notSeen:
        return tradesByTime.filter(
          (trade: Trade) => trade.hasNewMessages
        );
      case TradesFilterTypes.hasPaid:
        return tradesByTime.filter((trade: Trade) => trade.isPaid);
    }
    return [];
  }
);

export const selectCurrentUserName = createSelector(
  selectCurrentUser,
  (user: User) => user.name
);

export const selectCurrentUserId = createSelector(
  selectCurrentUser,
  (user: User) => user.id
);

export const selectUserRole = createSelector(
  selectCurrentUserId,
  selectTrades,
  (userId: string, trades: Trade[]) =>
    trades.some((trade: Trade) => trade.buyerId === userId)
      ? 'buyer'
      : 'seller'
);

export const selectIsUserBuyer = createSelector(
  selectCurrentUserId,
  selectTrades,
  (userId: string, trades: Trade[]): boolean =>
    trades.some((trade: Trade) => trade.buyerId === userId)
);

export const selectCurrentChatMessages = createSelector(
  selectCurrentTrade,
  (trade: Trade) => [...trade.chatMessages].reverse()
);

export const selectTraderUser = createSelector(
  selectCurrentTrade,
  selectCurrentUserId,
  selectAllUsers,
  (trade: Trade, userId: string, allUsers: User[]) => {
    const traderId =
      userId === trade.buyerId ? trade.sellerId : trade.buyerId;
    return allUsers.reduce((acc: User, user: User) => {
      if (user.id === traderId) return user;
      return acc;
    });
  }
);
