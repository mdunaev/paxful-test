import { Store } from 'redux';
import * as io from 'socket.io-client';

import { disconnectAction, newMessageAction, refreshStateAction, updateBTCPriceAction } from './actions';
import { ServerState, State } from './initialState';
import { selectRouterData } from './selectors';

const socket: SocketIOClient.Socket = io.connect(
  '/',
  {
    path: '/api/socket.io'
  }
);

// Transport
// Connect with server using Socket IO
// and handle store and socket connection in in module closure
export const transport = (store: Store<State>) => {
  socket.on('connect', () => {
    const { userId } = selectRouterData(store.getState());
    requestStateSocket(userId);
  });
  socket.on('disconnect', () => console.log('socket disconnected'));

  socket.on('full-state', (data: ServerState) => {
    store.dispatch(refreshStateAction(data));
  });

  socket.on('chat-message', (data: any) => {
    store.dispatch(
      newMessageAction({
        text: data.message,
        date: data.date,
        fromId: data.userId,
        tradeId: data.tradeId,
        isImage: data.isImage
      })
    );
  });

  socket.on('disconnected', () => store.dispatch(disconnectAction()));
  getExchangeRate(store);
};

const getExchangeRate = (store: Store<State>) => {
  fetch('https://api.coindesk.com/v1/bpi/currentprice/USD.json')
    .then(response => response.json())
    .then(data =>
      store.dispatch(updateBTCPriceAction(data.bpi.USD.rate_float))
    );
};

export const requestStateSocket = (userId?: string) => {
  socket.emit('get-user-state', { userId });
};

export const markTradeAsReadSocket = (tradeId: string) => {
  socket.emit('read-trade', { tradeId });
};

export const deleteTradeSocket = (tradeId: string) => {
  socket.emit('delete-trade', { tradeId });
};

export const payTradeSocket = (tradeId: string) => {
  socket.emit('pay-trade', { tradeId });
};

export const sendMessageSocket = (
  message: string,
  tradeId: string,
  userId: string,
  isImage: boolean = false
) =>
  socket.emit('chat-message', { message, tradeId, userId, isImage });
