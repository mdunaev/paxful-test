import * as faker from 'faker';

import { getRandomPayment } from './paymentMethods';

export interface ServerState {
  sellers: User[];
  buyers: User[];
  trades: Trade[];
}

export interface Trade {
  tradeId: string;
  buyerId: string;
  sellerId: string;
  paymentMethod: string;
  amount: number;
  isPaid: boolean;
  chatMessages: ChatMessage[];
  hasNewMessages: boolean;
  date: Date;
}

interface ChatMessage {
  date: Date;
  fromId: string;
  text: String;
  isImage: boolean;
}

interface User {
  id: string;
  name: string;
  positiveReputation: number;
  negativeReputation: number;
}

// Selectors
export const selectTradeUsersById = (
  state: ServerState,
  tradeId: string
) =>
  state.trades.reduce((acc: [], trade: Trade) => {
    if (tradeId === trade.tradeId)
      return [trade.buyerId, trade.sellerId];
    return acc;
  }, []);

export const selectRandomSellerId = (state: ServerState) =>
  state.sellers[~~(state.sellers.length * Math.random())].id;

export const getStateForId = (state: ServerState, id: string) => ({
  ...state,
  id,
  trades: state.trades.filter(
    (trade: Trade) => trade.sellerId === id || trade.buyerId === id
  )
});

export const selectIsUserExist = (
  state: ServerState,
  userId: string
): boolean =>
  [...state.buyers, ...state.sellers].some(
    (user: User) => user.id === userId
  );

// State generation
const generateName = () => faker.name.findName();
const generateId = () => faker.random.uuid().split('-')[0];
const generateSentence = () => faker.lorem.sentence();
const generateReputation = () => Math.floor(100 * Math.random());

const generateUser = () => ({
  id: generateId(),
  name: generateName(),
  positiveReputation: generateReputation() * 3,
  negativeReputation: -generateReputation()
});

const generateUsers = (size: number): User[] =>
  [...Array(size)].map(generateUser);

const generateTrade = (seller: User, buyer: User): Trade => ({
  sellerId: seller.id,
  buyerId: buyer.id,
  tradeId: generateId(),
  paymentMethod: getRandomPayment(),
  amount: ~~(Math.random() * 200),
  isPaid: Math.random() > 0.5,
  chatMessages: [],
  hasNewMessages: Math.random() > 0.5,
  date: new Date(new Date().getTime() - Math.random() * 10000000)
});

const generateTrades = (sellers: User[], buyers: User[]): Trade[] => {
  let trades = [];
  sellers.map((seller: User) => {
    const tradesNum = ~~(Math.random() * 12 + 2);
    const shuffledBuyers = buyers
      .sort(() => 0.5 - Math.random())
      .slice(0, tradesNum);
    shuffledBuyers.map((buyer: User) => {
      trades.push(generateTrade(seller, buyer));
    });
  });
  return trades;
};

const generateMessage = (trade: Trade): ChatMessage => ({
  date: new Date(),
  text: generateSentence(),
  isImage: false,
  fromId: Math.random() > 0.5 ? trade.buyerId : trade.sellerId
});

const applyMessagesToTrades = (trades: Trade[]): Trade[] =>
  trades.map((trade: Trade) => ({
    ...trade,
    chatMessages: [...Array(~~(Math.random() * 10 + 4))].map(() =>
      generateMessage(trade)
    )
  }));

export const generateInitialState = (): ServerState => {
  const sellers: User[] = generateUsers(10);
  const buyers: User[] = generateUsers(10);
  const tradesTemplates: Trade[] = generateTrades(sellers, buyers);
  const trades: Trade[] = applyMessagesToTrades(tradesTemplates);
  return {
    sellers,
    buyers,
    trades
  };
};

// State mutation
export const addChatMessageToState = (
  state: ServerState,
  tradeId: string,
  userId: string,
  message: string,
  isImage: boolean
): ServerState => {
  return {
    ...state,
    trades: state.trades.map((trade: Trade) => {
      if (trade.tradeId === tradeId) {
        trade.chatMessages.unshift({
          date: new Date(),
          fromId: userId,
          text: message,
          isImage: isImage
        });
        return {
          ...trade,
          chatMessages: trade.chatMessages.slice(0, 20),
          hasNewMessages: true
        };
      }
      return trade;
    })
  };
};

export const markTradeAsRead = (
  state: ServerState,
  tradeId: string
) => {
  return {
    ...state,
    trades: state.trades.map((trade: Trade) => {
      if (trade.tradeId === tradeId) {
        return {
          ...trade,
          hasNewMessages: false
        };
      }
      return trade;
    })
  };
};

export const deleteTrade = (state: ServerState, tradeId: string) => {
  return {
    ...state,
    trades: [
      ...state.trades.filter(
        (trade: Trade) => trade.tradeId !== tradeId
      ),
      applyMessagesToTrades([
        generateTrade(getRandomSeller(state), getRandomBuyer(state))
      ])[0]
    ]
  };
};

const getRandomSeller = (state: ServerState) =>
  state.sellers[Math.floor(state.sellers.length * Math.random())];
const getRandomBuyer = (state: ServerState) =>
  state.buyers[Math.floor(state.buyers.length * Math.random())];

export const payTrade = (state: ServerState, tradeId: string) => {
  return {
    ...state,
    trades: state.trades.map((trade: Trade) => {
      if (trade.tradeId === tradeId) {
        return {
          ...trade,
          isPaid: true
        };
      }
      return trade;
    })
  };
};
